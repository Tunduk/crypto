﻿using System;
using System.Text.RegularExpressions;
using Apex.MVVM;
using System.Windows.Input;
using Сryptography.CryptoServices;

namespace Сryptography.ViewModels
{
    public class XorCryptoViewModel : BaseCryptoViewModel
    {
        private readonly XORCryptoService _cryptoService;
        private string _inputText;
        private string _outputText;
        private string _key;
        private string _error;
        private bool _isEnableButton;

        public string Error
        {
            get => _error;
            set { _error = value; OnPropertyChanged(nameof(Error)); }
        }

        public string InputText
        {
            get => _inputText;
            set { _inputText = value; OnPropertyChanged(nameof(InputText)); }
        }

        public string OutputText
        {
            get => _outputText;
            set { _outputText = value; OnPropertyChanged(nameof(OutputText)); }
        }

        public string Key
        {
            get => _key;
            set
            { _key = value; OnPropertyChanged(nameof(Key)); }
        }
        public bool IsEnableButton
        {
            get => _isEnableButton;
            set
            {
                _isEnableButton = value; OnPropertyChanged(nameof(IsEnableButton));
            }
        }

        public ICommand Encrypt { get; internal set; }
        public ICommand CheckText { get; internal set; }

        public XorCryptoViewModel()
        {
            IsEnableButton = false;
            _cryptoService = new XORCryptoService();
            InputText = String.Empty;
            Key = String.Empty;
            Encrypt = new Command(EncryptExecute);
            CheckText = new Command(CheckTextExecute);
        }

        private void EncryptExecute()
        {
            OutputText = _cryptoService.Encrypt(_inputText, _key);
        }

        private void CheckTextExecute()
        {
            var regex = "[^0-1]";
            if (Regex.IsMatch(InputText, regex))
            {
                Error = "Сообщение должно состоять из '0' и '1'";
                IsEnableButton = false;
                return;
            }
            if (Regex.IsMatch(Key, regex))
            {
                Error = "Ключ должен состоять из '0' и '1'";
                IsEnableButton = false;
                return;
            }

            if (string.IsNullOrWhiteSpace(Key))
            {
                Error = "Необходимо ввести ключ";
                IsEnableButton = false;
                return;
            }

            if (string.IsNullOrWhiteSpace(InputText))
            {
                Error = "Необходимо ввести сообщение";
                IsEnableButton = false;
                return;
            }
            Error = String.Empty;
            IsEnableButton = true;
        }
    }
}
