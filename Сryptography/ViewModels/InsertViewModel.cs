﻿using System;
using System.Linq;
using System.Windows.Input;
using Apex.MVVM;
using Сryptography.CryptoServices;

namespace Сryptography.ViewModels
{
    public class InsertViewModel : BaseCryptoViewModel
    {
        private readonly InsertCryptoService _cryptoService;

        private string _normalAlphabet;
        private string _randomAlphabet;
        private string _inputText;
        private string _outputText;
        private string _error;
        private bool _isEnableButton;
        public bool IsEnableButton
        {
            get => _isEnableButton;
            set
            {
                _isEnableButton = value; OnPropertyChanged(nameof(IsEnableButton));
            }
        }

        public string NormalAlphabet
        {
            get => _normalAlphabet;
            set { _normalAlphabet = value; OnPropertyChanged(nameof(NormalAlphabet)); }
        }

        public string Error
        {
            get => _error;
            set { _error = value; OnPropertyChanged(nameof(Error)); }
        }

        public string RandomAlphabet
        {
            get => _randomAlphabet;
            set { _randomAlphabet = value; OnPropertyChanged(nameof(RandomAlphabet)); }
        }


        public string InputText
        {
            get => _inputText;
            set { _inputText = value; OnPropertyChanged(nameof(InputText)); }
        }

        public string OutputText
        {
            get => _outputText;
            set { _outputText = value; OnPropertyChanged(nameof(OutputText)); }
        }

        public ICommand Encrypt { get; internal set; }
        public ICommand CreateAlphabet { get; internal set; }
        public ICommand DeCrypt { get; internal set; }
        public ICommand CheckAlphabets { get; internal set; }
        public ICommand CheckText { get; internal set; }

        public InsertViewModel()
        {
            IsEnableButton = false;
            _cryptoService = new InsertCryptoService();
            NormalAlphabet = "Перед началом шифрования нажмите кнопку \"Перемешать\" или введите свой алфавит";
            RandomAlphabet = String.Empty;
            InputText = String.Empty;
            CreateAlphabet = new Command(CreateAlpabetExecute);
            Encrypt = new Command(EncryptExecute);
            DeCrypt = new Command(DeCryptExecute);
            CheckAlphabets = new Command(CheckAlphabetsExecute);
            CheckText = new Command(CheckTextExecute);
        }

        private void EncryptExecute()
        {

            OutputText = _cryptoService.GetCryptoText(NormalAlphabet, RandomAlphabet, InputText);
        }

        private void CreateAlpabetExecute()
        {
            if (String.IsNullOrEmpty(NormalAlphabet))
            {
                RandomAlphabet = _cryptoService.MakeRandomAlphabet();
                NormalAlphabet = _cryptoService.DefaultRussianAlphabet;
            }
            else
            {
                NormalAlphabet = NormalAlphabet.Replace(" ", "");
                RandomAlphabet = _cryptoService.MakeRandomAlphabet(NormalAlphabet);
            }
        }

        private void DeCryptExecute()
        {
            OutputText = _cryptoService.DeCrypto(NormalAlphabet, RandomAlphabet, InputText);
        }

        private void CheckAlphabetsExecute()
        {
            if (String.IsNullOrWhiteSpace(NormalAlphabet))
            {
                Error = "Введите исходный алфавит!";
                IsEnableButton = false;
                return;
            }

            if (String.IsNullOrWhiteSpace(RandomAlphabet))
            {
                Error = "Введите зашифрованный алфавит!";
                IsEnableButton = false;
                return;
            }

            if (NormalAlphabet.Where((t, i) => NormalAlphabet.LastIndexOf(t) != i && !Char.IsPunctuation(t)).Any())
            {
                Error = "В исходном алфавите не должно быть повторов!";
                IsEnableButton = false;
                return;
            }

            if (RandomAlphabet.Where((t, i) => RandomAlphabet.LastIndexOf(t) != i && !Char.IsPunctuation(t)).Any())
            {
                Error = "В зашифрованном алфавите не должно быть повторов!";
                IsEnableButton = false;
                return;
            }

            if (NormalAlphabet.Any(t => !RandomAlphabet.Contains(t)))
            {
                Error = "Зашифрованный алфавит должен содержать все символы исходного!";
                IsEnableButton = false;
                return;
            }

            IsEnableButton = true;
            Error = String.Empty;
            CheckTextExecute();
        }

        private void CheckTextExecute()
        {
            if (String.IsNullOrWhiteSpace(InputText))
            {
                Error = "Введите шифруемое сообщение!";
                IsEnableButton = false;
                return;
            }

            if (InputText.Any(t => !NormalAlphabet.Contains(t) && !char.IsControl(t)))
            {
                Error = "Шифруемый текст должен содержать все символы исходного!";
                IsEnableButton = false;
                return;
            }

            IsEnableButton = true;
            Error = String.Empty;
        }
    }
}
