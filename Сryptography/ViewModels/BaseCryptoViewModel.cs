﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Сryptography.Annotations;

namespace Сryptography.ViewModels
{
    public abstract class BaseCryptoViewModel:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
