﻿using System;
using System.Windows;
using Сryptography.ViewModels;

namespace Сryptography.Views
{
    /// <summary>
    /// Логика взаимодействия для InsertCrypto.xaml
    /// </summary>
    public partial class InsertCrypto
    {
        public InsertCrypto()
        {
            InitializeComponent();
            DataContext = new InsertViewModel();
        }

        private void Back(object sender, RoutedEventArgs args)
        {
            NavigationService?.Navigate(new Uri("../MainPage.xaml", UriKind.Relative));
        }
    }
}
