﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Сryptography.CryptoServices;

namespace Сryptography.Views
{
    /// <summary>
    /// Логика взаимодействия для CesarCrypto.xaml
    /// </summary>
    public partial class CesarCrypto : Page
    {
        private CesarCryptoService _cesarCryptoService;
        public CesarCrypto()
        {
            InitializeComponent();
            _cesarCryptoService = new CesarCryptoService();
        }

        private void Crypto(object sender, RoutedEventArgs args)
        {
            if (!String.IsNullOrWhiteSpace(OffsetInput.Text) && OffsetInput.Text.All(x => Char.IsDigit(x)))
            {
                if (!AlphabetInput.Text.Where((t, i) => AlphabetInput.Text.LastIndexOf(t) != i && !Char.IsPunctuation(t)).Any())
                {
                    WarningOffsetLabel.Text = String.Empty;
                    var t = AlphabetInput.Text.Split(',');
                    var alp = string.Empty;
                    foreach (var st in t)
                    {
                        alp = $"{alp}{st}";
                    }
                    _cesarCryptoService.NormalAlphabet = alp;
                    CryptoTextOutput.Text = _cesarCryptoService.GetCryptoText(Int32.Parse(OffsetInput.Text), NormalTextInput.Text);
                }
                else
                {
                    WarningOffsetLabel.Text = "В исходном алфавите не должно быть повторов!";
                }
            }
            else
            {
                WarningOffsetLabel.Text = "Шаг смещения должен состоять только из цифр и не иметь пробелов!";
            }

        }
        //TODO:Check input data
        private void Encrypt(object sender, RoutedEventArgs args)
        {
            if (!String.IsNullOrWhiteSpace(OffsetInput.Text) && OffsetInput.Text.All(x => Char.IsDigit(x)))
            {
                if (!AlphabetInput.Text.Where((t, i) => AlphabetInput.Text.LastIndexOf(t) != i && !Char.IsPunctuation(t)).Any())
                {
                    WarningOffsetLabel.Text = String.Empty;
                    var t = AlphabetInput.Text.Split(',');
                    var alp = string.Empty;
                    foreach (var st in t)
                    {
                        alp = $"{alp}{st}";
                    }
                    _cesarCryptoService.NormalAlphabet = alp;
                    CryptoTextOutput.Text = _cesarCryptoService.GetEncryptText(Int32.Parse(OffsetInput.Text), NormalTextInput.Text);
                }
                else
                {
                    WarningOffsetLabel.Text = "В исходном алфавите не должно быть повторов!";
                }
            }
            else
            {
                WarningOffsetLabel.Text = "Шаг смещения должен состоять только из цифр и не иметь пробелов!";
            }
        }
        private void Back(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("MainPage.xaml", UriKind.Relative));
        }

        private void OffsetInput_GotFocus(object sender, RoutedEventArgs e)
        {
            var o = sender as TextBox;
            o.Text = String.Empty;
        }
    }
}
