﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using Сryptography.ViewModels;

namespace Сryptography.Views
{
    /// <summary>
    /// Логика взаимодействия для XORCrypto.xaml
    /// </summary>
    public partial class XORCrypto 
    {
        public XORCrypto()
        {
            InitializeComponent();
            DataContext = new XorCryptoViewModel();
        }

        private void Back(object sender, RoutedEventArgs e)
        {
            NavigationService?.Navigate(new Uri("../MainPage.xaml", UriKind.Relative));
        }

        //private void Crypto(object sender, RoutedEventArgs e)
        //{
        //    if (CheckInput(TI.Text, KeyInput.Text))
        //        CryptoTB.Text = _cryptoService.Encrypt(TI.Text, KeyInput.Text);
        //}

    }
}
