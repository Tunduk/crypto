﻿using System;
using System.Windows;
using System.Windows.Controls;
using Сryptography.CryptoServices;

namespace Сryptography.Views
{
    /// <summary>
    /// Логика взаимодействия для HardBlock.xaml
    /// </summary>
    public partial class HardBlock : Page
    {
        private HardBlockCryptoService _cryptoService;
        public HardBlock()
        {
            InitializeComponent();
            _cryptoService = new HardBlockCryptoService();
        }


        private void Crypto(object sender, RoutedEventArgs args)
        {
           // CryptoTB.Text = _cryptoService.Encrypt(TI.Text, KeyInput.Text);
        }
        private void Decrypto(object sender, RoutedEventArgs args)
        {
           // CryptoTB.Text = _cryptoService.Decrypt(TI.Text, KeyInput.Text);
        }

        private void Back(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("../MainPage.xaml", UriKind.Relative));
        }

    }
}
