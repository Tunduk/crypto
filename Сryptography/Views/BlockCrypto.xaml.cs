﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Сryptography.CryptoServices;

namespace Сryptography.Views
{
    /// <summary>
    /// Логика взаимодействия для BlockCrypto.xaml
    /// </summary>
    public partial class BlockCrypto : Page
    {
        private BlockCryptoService _cryptoService;
        public BlockCrypto()
        {
            InitializeComponent();
            _cryptoService = new BlockCryptoService();
        }


        private void Crypto(object sender, RoutedEventArgs args)
        {
            if (KeyInput.Text.Length >= Int32.Parse(KeyInput.Text.Max().ToString()))
            {
                if (!KeyInput.Text.Where((t, i) => KeyInput.Text.LastIndexOf(t) != i).Any())
                {
                    CryptoTB.Text = _cryptoService.Encrypt(TI.Text, KeyInput.Text);
                    
                }
            }
        }
        private void Decrypto(object sender, RoutedEventArgs args)
        {
            CryptoTB.Text = _cryptoService.Decrypt(TI.Text, KeyInput.Text);
        }

        private void Back(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("MainPage.xaml", UriKind.Relative));
        }
    }
}
