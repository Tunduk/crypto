﻿using System;
using System.Windows;

namespace Сryptography
{
    /// <summary>
    /// Логика взаимодействия для MainPage.xaml
    /// </summary>
    public partial class MainPage 
    {
        
        public MainPage()
        {
            InitializeComponent();
            
        }
        private void OpenInsertCrypto(object sender, RoutedEventArgs e)
        {
            NavigationService?.Navigate(new Uri("/Views/InsertCrypto.xaml", UriKind.Relative));
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void OpenCesarCrypto(object sender, RoutedEventArgs e)
        {
            NavigationService?.Navigate(new Uri("/Views/CesarCrypto.xaml", UriKind.Relative));
        }

        private void OpenXORCrypto(object sender, RoutedEventArgs e)
        {
            NavigationService?.Navigate(new Uri("/Views/XORCrypto.xaml", UriKind.Relative));
        }

        private void OpenBlockCrypto(object sender, RoutedEventArgs e)
        {
            NavigationService?.Navigate(new Uri("/Views/BlockCrypto.xaml", UriKind.Relative));
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService?.Navigate(new Uri("/Views/HardBlock.xaml", UriKind.Relative));
        }
    }
}
