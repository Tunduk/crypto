﻿using System;
using System.Linq;

namespace Сryptography.CryptoServices
{
    public class BlockCryptoService
    {
        public string Encrypt(string source, string block)
        {
            var temp = string.Empty;
            while (source != string.Empty)
            {
                foreach (char с in block)
                {
                    temp = $"{temp}{source[(int)(Int32.Parse(с.ToString()) - 1)]}";
                }
                source = source.Substring(block.Length);
            }


            return temp;
        }

        public string Decrypt(string source, string block)
        {
            var temp = source;
            var count = block.Select(t => (int)(Char.GetNumericValue(t) - 1)).Where((index, i) => index == i).Count();
            for (var i = 0; i < block.Length - 1 - count; i++)
            {
                temp = Encrypt(temp, block);
            }
            return temp;
        }
    }
}
