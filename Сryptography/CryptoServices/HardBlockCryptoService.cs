﻿using System;

namespace Сryptography.CryptoServices
{
    public class HardBlockCryptoService
    {
        public HardBlockCryptoService()
        {

        }


        public string[] hard_block_enc(string key, string input_text)
        {

            //проверка на значение ключа

            System.Int32 a;
            if (!int.TryParse(key, out a))
            {
               // MessageBox.Show("Для правильной работы этого алгоритма ключ должен быть числовым значением, не превышающим 9", "Ошибка", MessageBoxButtons.OK);
                string[] answer = new string[1];
                answer[0] = "";
                return answer;
            }

            string[] output_text = new string[512];
            int i, j;

            //парсим ключ

            int block_size = key.Length;
            int[] block_order = new int[block_size];
            for (i = 0; i < block_size; i++)
                block_order[i] = (int)char.GetNumericValue(key[i]);

            //конвертируем весь текст в одну строку

            string tmp_str_input_text = "";

            for (i = 0; i < input_text.Length; i++)
                tmp_str_input_text += input_text[i] + ' ';

            int tail_l = tmp_str_input_text.Length % block_size;
            int enc_part_l = tmp_str_input_text.Length - tail_l;

            char[] str_input_text = new char[enc_part_l];
            char[] tail = new char[tail_l];

            for (i = 0; i < tmp_str_input_text.Length; i++)
                if (i < enc_part_l)
                    str_input_text[i] = tmp_str_input_text[i];
                else
                    tail[i - enc_part_l] = tmp_str_input_text[i];

            //основной алгоритм

            int k = 0;

            for (i = 0; i < str_input_text.Length; i += block_size)
            {
                char[] block = new char[block_size]; //массив под текущий блок
                char[] temp_block = new char[block_size];

                for (j = i; j < i + block_size; j++)
                    block[j - i] = str_input_text[j];

                for (j = i; j < i + block_size; j++)
                    temp_block[j - i] = str_input_text[j];

                for (j = 0; j < block_size; j++)
                    block[j] = temp_block[block_order[j] - 1];

                if (k % 2 != 0) Array.Reverse(block);
                output_text[k] = new string(block);
                k++;
            }

            if (k % 2 != 0) Array.Reverse(tail);
            output_text[k] = new string(tail); //перевернуть строку слева направо
            return output_text;
        }
    }
}
