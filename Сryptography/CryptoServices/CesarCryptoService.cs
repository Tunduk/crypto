﻿using System;
using System.Linq;

namespace Сryptography.CryptoServices
{
    public class CesarCryptoService
    {
        public string CryptoAlphabet
        {
            get;
            set;
        }
        public string NormalAlphabet
        {
            get;
            set;
        }

        public CesarCryptoService()
        {
            NormalAlphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            CryptoAlphabet = String.Empty;
        }

        public string GetCryptoText(int offset, string text)
        {
            var outputText = String.Empty;
            if (offset >= NormalAlphabet.Count())
            {
                while (offset >= NormalAlphabet.Length)
                {
                    offset -= NormalAlphabet.Length;
                }
            }
            CryptoAlphabet = String.Format("{0}{1}", NormalAlphabet.Substring(offset), NormalAlphabet.Substring(0, offset));
            foreach (var letter in text)
            {
                if (Char.IsUpper(letter))
                {
                    outputText = String.Format("{0}{1}", outputText, Char.ToUpper(CryptoAlphabet[NormalAlphabet.IndexOf(Char.ToLower(letter))]));
                }
                else
                {
                    outputText = String.Format("{0}{1}", outputText, CryptoAlphabet[NormalAlphabet.IndexOf(letter)]);
                }

            }
            return outputText;
        }

        public string GetEncryptText(int offset, string cryptoText)
        {
            var outputText = String.Empty;
            if (offset >= NormalAlphabet.Count())
            {
                while (offset >= NormalAlphabet.Length)
                {
                    offset -= NormalAlphabet.Length;
                }
            }

            CryptoAlphabet = String.Format("{0}{1}", NormalAlphabet.Substring(offset), NormalAlphabet.Substring(0, offset));
            foreach (var letter in cryptoText)
            {

                if (Char.IsUpper(letter))
                {
                    outputText = String.Format("{0}{1}", outputText, Char.ToUpper(NormalAlphabet[CryptoAlphabet.IndexOf(Char.ToLower(letter))]));
                }
                else
                {
                    outputText = String.Format("{0}{1}", outputText, NormalAlphabet[CryptoAlphabet.IndexOf(letter)]);
                }
            }
            return outputText;
        }

    }
}

