﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Сryptography.CryptoServices
{
    public class InsertCryptoService
    {
        private readonly string _defaultRussianAlphabet;

        public string DefaultRussianAlphabet => _defaultRussianAlphabet;

        public InsertCryptoService()
        {
            _defaultRussianAlphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        }

        public string MakeRandomAlphabet()
        {
            return MakeRandomAlphabet(_defaultRussianAlphabet);
        }

        public string MakeRandomAlphabet(string alphabet)
        {
            var random = new Random();
            for (var i = 0; i < 1000; i++)
            {
                var r = random.Next(alphabet.Length);
                var temp = alphabet[r];
                alphabet = alphabet.Remove(r, 1);
                alphabet = alphabet.Insert(random.Next(alphabet.Length), temp.ToString());
            }
            return alphabet;
        }

        public string GetCryptoText(string alphabet, string randomAlphabet, string text)
        {
            List<string> a = new List<string>(alphabet.Split(',')) {"\n", "\r"};
            List<string> b = new List<string>(randomAlphabet.Split(',')) {"\n", "\r"};

            return text.Aggregate(String.Empty, (current, t) => $"{current}{b[a.IndexOf(t.ToString())]}");
        }
        public string DeCrypto(string alphabet, string randomAlphabet, string text)
        {
            return text.Aggregate(String.Empty, (current, t) => $"{current}{alphabet[randomAlphabet.IndexOf(t)]}");
        }
    }
}
