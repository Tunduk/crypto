﻿using System;

namespace Сryptography.CryptoServices
{
    public class XORCryptoService
    {

        public string Encrypt(string source, string key)
        {
            var longkey = string.Empty;
            for (int i = 0; i < source.Length - 1 / key.Length - 1; i++)
                longkey = $"{longkey}{key}";
            var result = String.Empty;
            for (int i = 0; i < source.Length; i++)
                result = $"{result}{source[i] ^ longkey[i]}";
            
            return result;
        }
    }
}
